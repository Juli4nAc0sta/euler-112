from itertools import count


def is_bouncy(n):
    increasing = True
    decreasing = True
    last = n % 10
    n //= 10
    while n > 0:
        new = n % 10
        if increasing and new > last:
            increasing = False

        if decreasing and new < last:
            decreasing = False
        last = new
        n //= 10
    return not (increasing or decreasing)


def check_ratio(ratio):
    counter = 0
    for i in count(1):
        if is_bouncy(i):
            counter += 1
        if (100*counter)//i >= ratio:
            print('least number for which the proportion of bouncy numbers is '
                  'exactly 99% is: {}'.format(i))
            return i


check_ratio(99)
